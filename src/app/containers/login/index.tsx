import * as React from "react";
import { Button, FormControl } from "react-bootstrap";
import { connect } from "react-redux";
import { RouteComponentProps } from "react-router";
import { push } from "react-router-redux";

import { login } from "app/actions/login";
import { IUser } from "app/models/user";

const style = require("./style.scss");

export interface ILoginProps extends RouteComponentProps<void> {

}

export interface ILoginStateProps {

}

export interface ILoginDispatchProps {
    login: typeof login;
    goToHome: () => void;
}

type TLoginProps = ILoginProps & ILoginStateProps & ILoginDispatchProps;

class Login extends React.PureComponent<TLoginProps, {}> {

    private handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        const user: IUser = {
            username: "admin",
            password: "welcome1"
        };
        this.props.login(user);
    }

    render() {
        return (
            <div className={style.loginContainer}>
            <h1> Login </h1>
                <form name="login" onSubmit={this.handleSubmit}>
                    <FormControl
                        type="text"
                        placeholder="Username"
                        className={style.form_control}
                    />
                    <FormControl
                        type="text"
                        placeholder="Password"
                        className={style.form_control}
                    />
                    <Button className={style.form_control} type="submit">LOGIN</Button>
                    <Button className={style.button} onClick={this.props.goToHome}>HOME</Button>
                </form>
            </div>
        );
    }
}

function mapStateToProps(): ILoginStateProps {
    return {

    };
}

function mapDispatchToProps(dispatch: any): ILoginDispatchProps {
    return {
        login: (user: IUser) => dispatch(login(user)),
        goToHome: () => dispatch(push("/home"))
    };
}

export default connect<ILoginStateProps, ILoginDispatchProps, ILoginProps>(mapStateToProps, mapDispatchToProps)(Login);